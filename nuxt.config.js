export default {
  mode: "universal",

  /*
   ** Headers of the page
   */
  head: {
    title: "InterHack",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content:
          "Inter-disciplinar. Inter-campi. Inter-cursos. InterHack. Participe do maior hackathon universitário do Brasil"
      },
      {
        property: "og:title",
        content: "InterHack"
      },
      {
        property: "og:description",
        content:
          "Inter-disciplinar. Inter-campi. Inter-cursos. InterHack. Participe do maior hackathon universitário do Brasil"
      },
      {
        property: "og:image",
        content:
          "https://firebasestorage.googleapis.com/v0/b/uclsanca.appspot.com/o/interhack.jpg?alt=media&token=9767a2ad-ab87-4f2f-82f7-45dee7978873"
      },
      {
        property: "og:image:type",
        content: "image/jpeg"
      },
      {
        property: "og:image:width",
        content: "800"
      },
      {
        property: "og:image:height",
        content: "600"
      },
      { property: "apple-mobile-web-app-title", content: "InterHack" },
      { property: "application-name", content: "InterHack" },
      { property: "msapplication-TileColor", content: "#ffffff" },
      { property: "msapplication-config", content: "/favicon/browserconfig.xml" },
      { property: "theme-color", content: "#ffffff" },
    ],
    link: [
      { rel: "apple-touch-icon", sizes: "180x180", href: "/favicon/apple-touch-icon.png" },
      { rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon/favicon-32x32.png" },
      { rel: "icon", type: "image/png", sizes: "16x16", href: "/favicon/favicon-16x16.png" },
      { rel: "mask-icon", href: "/favicon/safari-pinned-tab.svg", color: "#f82947" },
      { rel: "shortcut icon", href: "/favicon/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Montserrat"
      },
      {
        rel: "stylesheet",
        href: "https://use.fontawesome.com/releases/v5.8.1/css/all.css",
        integrity:
          "sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf",
        crossorigin: "anonymous"
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },

  /*
   ** Global CSS
   */
  css: ["~/assets/css/tailwind.css"],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: "plugins/vue-typer.js", ssr: false },
    { src: "plugins/vue-smooth-scroll.js", ssr: false },
    { src: "plugins/vue-carousel.js", ssr: false }
  ],

  /*
   ** Nuxt.js modules
   */
  modules: ["@nuxtjs/pwa"],

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) { }
  }
};
